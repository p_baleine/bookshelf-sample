var _ = require('lodash'),
    BaseModel = require('./base'),
    Bookshelf = require('bookshelf').sampleBookshelf,
    Comment = require('./comment'),
    Tag = require('./tag');

var Post = module.exports = BaseModel.extend({
  tableName: 'posts',
  comments: function() {
    return this.hasMany(Comment);
  },
  tags: function() {
    return this.belongsToMany(Tag);
  },
  // override
  save: function(params, options) {
    var _this = this;

    return Bookshelf.transaction(function(t) {
      var tags = _this.get('tags');

      _this.attributes = _this.omit('tags');
      options = _.extend(options || {}, { transacting: t });

      return BaseModel.prototype.save.call(_this, params, options)
        .then(function() {
          // この投稿に紐付くタグを一旦全部削除
          return _this.tags().detach(null, options);
        })
        .then(function() {
          // 引数にもらったタグを追加
          return _this.tags().attach(tags, options);
        })
        .then(t.commit, t.rollback);
    }).yield(this);
  }
});
