var Bookshelf = require('bookshelf'),
    databaseConfig = require('./db/config').database,

    sampleBookshelf = Bookshelf.sampleBookshelf = Bookshelf.initialize(databaseConfig);

var BaseModel = module.exports = sampleBookshelf.Model.extend({
  hasTimestamps: true
}, {
  findAll: function() {
    var coll = sampleBookshelf.Collection.forge([], { model: this });
    return coll.fetch.apply(coll, arguments);
  }
});
