var BaseModel = require('./base');

var Comment = module.exports = BaseModel.extend({
  tableName: 'comments'
});
