module.exports = {
  directory: './db/migrations',
  database: {
    client: 'sqlite3',
    connection: {
      filename: './db/sample.sqlite3'
    }
  }
};
