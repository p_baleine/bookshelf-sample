
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('tags', function(t) {
      t.increments().primary();
      t.string('text').notNullable();
      t.timestamps();
    }),

    knex.schema.createTable('posts_tags', function(t) {
      t.increments().primary();
      t.integer('post_id').notNull().references('id').inTable('posts');
      t.integer('tag_id').notNull().references('id').inTable('tags');
      t.unique(['post_id', 'tag_id'], 'post_tag_index');
    })  
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('tags'),
    knex.schema.dropTable('posts_tags')
  ]);  
};
