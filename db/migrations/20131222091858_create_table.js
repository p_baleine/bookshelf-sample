
exports.up = function(knex, Promise) {
  return Promise.all([
    // postsテーブル
    knex.schema.createTable('posts', function(t) {
      t.increments('id').primary();
      t.string('title').notNullable();
      t.string('content').notNullable();
      t.timestamps();
    }),
    // commentsテーブル
    knex.schema.createTable('comments', function(t) {
      t.increments('id').primary();
      t.string('commenter').notNullable();
      t.string('content').notNullable();
      t.timestamps();
      t.integer('post_id').notNull().references('id').inTable('posts');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('posts'),
    knex.schema.createTable('comments')    
  ]);
};
